
export const getLevels = () => {
    const levels = [
        { level: 1, desc: "Thor", sets: 3 },
        { level: 2, desc: "Hulk", sets: 5 },
    ];

    return levels;
}