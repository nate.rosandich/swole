
export const getExercises = () => {
    const exercises = [
        {
            name: 'Squats',
            image: '/exercises/barbell.png',
            desc: 'Stand straight with feet hip-width apart. The barbell should rest on the muscles of your upper back. Tighten your stomach muscles. Lower down, as if sitting in an invisible chair. Straighten your legs to lift back up. Repeat the movement.',
        },
        {
            name: 'Bench Press',
            image: '/exercises/bench.png',
            desc: 'Lie on your back on a flat bench. Grip a barbell with hands slightly wider than shoulder width. Lower the bar to the chest, allowing elbows to bend out to the side. Press feet into the floor as you push the bar back up to return to starting position.',
        },
        {
            name: 'Deadlift',
            image: '/exercises/deadlift.png',
            desc: 'Standing with your feet shoulder-width apart, grasp the bar with your hands just outside your legs. Lift the bar by driving your hips forwards and pushing feet down, keeping a flat back. Breath out on way up and squeeze buttocks. Lower the bar under control',
        },
    ];
    //pics https://thenounproject.com/creativestall/collection/pictograms-line-icons/

    return exercises;

}