import { reactive } from "vue";
import { getLevels, getExercises } from "@/services/data";

const current = reactive({
    level: 1,
    index: 0,
    sets: 0,
    exercises: [],
});

const timing = reactive({
    start: null,
    finish: null,
    time: null
});

export const useExercise = () => {

    const levels = getLevels();
    const exercises = getExercises();

    const setExercises = ({ setLevel = 1 }) => {

        timing.value = 0;

        current.level = setLevel;
        current.index = 0;
        current.exercises = [];

        var pickedLevel = levels.filter(level => {
            return level.level === setLevel
        });

        const sets = pickedLevel[0].sets;
        current.sets = sets;

        if (sets && sets > 0) {
            exercises.forEach(exercise => {
                for (let i = 0; i < sets; i++) {
                    current.exercises.push({
                        ...exercise,
                        name: `${exercise.name}`,
                        set: i + 1,
                    });
                }
            });
        }
    }

    const setIndex = ({ index = 0 } = {}) => {
        current.index = index;
    }

    const reset = () => {
        current.level = 1;
        current.index = 0;
        current.exercises = [];
        current.sets = 0;
        timing.start = null;
        timing.finish = null;
        timing.time = null;
    }

    return {
        levels,
        exercises,
        current,
        timing,
        reset,
        setIndex,
        setExercises,
    };
}