import { ref } from "vue";
import { log, stringToSlug, checkArrayOfObjectsByKey } from "@/utils";
import { addSession, addData, getDataSource, addDataSource } from "@/services/googleFit";

//Based on https://developers.google.com/fit/rest/v1/workout

const dataStreamId = ref("derived:com.google.activity.segment:257811618614:Swole");
const baseUrl = `https://www.googleapis.com/fitness/v1/users/me/`;

export const useGoogleFit = () => {

    const sendIt = async ({ access_token = null, start = null, finish = null, title = null } = {}) => {
        try {

            //First we need to check that the data source is avialable to this person
            const dataSourceResponse = await getDataSource({ baseUrl, accessToken: access_token });
            log({ message: "Get data source response", data: dataSourceResponse });

            const dataSourceCheck = checkArrayOfObjectsByKey({ checkArray: dataSourceResponse.dataSource, key: 'dataStreamId', value: dataStreamId.value });
            log({ message: "Check data source is in response", data: dataSourceCheck });

            //If not avalable then we add a new one and update internal data sourcce
            if (dataSourceCheck === undefined) {
                const newdataSourceResponse = await addDataSource({ baseUrl, accessToken: access_token });
                dataStreamId.value = newdataSourceResponse.dataStreamId;
                log({ message: "Create data source", data: newdataSourceResponse });
            }

            //Now we create the actual data points
            const id = stringToSlug({ string: `swole-${title}-${start}` });
            const dataResponse = await addData({ baseUrl, accessToken: access_token, datasource: dataStreamId.value, start, finish });
            // fake response for testing // const dataResponse = { error: "test" };
            log({ message: "Create data points", data: dataResponse });

            //If issue adding the above data point we through error
            if (dataResponse?.error) {
                throw new Error('Google fit data points not created');
            }

            //Add the session
            const sessionResponse = await addSession({ baseUrl, accessToken: access_token, id, start, finish, title });
            log({ message: "Create session", data: sessionResponse });
            return Promise.resolve(sessionResponse);

        } catch (error) {
            log({ message: "Google connect error;", data: error, level: "error" });
            return Promise.reject(error);
        }
    }

    return {
        sendIt,
    };
}