import { ref } from "vue";
import { log } from "@/utils";
import { revokeAccessToken, useTokenClient } from "vue3-google-signin";

const accessToken = ref(JSON.parse(localStorage.getItem("accessToken")));
const googleEmail = ref(JSON.parse(localStorage.getItem("googleEmail")));

export const useGoogle = () => {

    const setRefs = ({ access_token = null, email = null } = {}) => {
        accessToken.value = access_token;
        localStorage.setItem("accessToken", JSON.stringify(access_token));
        googleEmail.value = email;
        localStorage.setItem("googleEmail", JSON.stringify(email));
    }

    const getProfile = async ({ access_token = null }) => {
        try {
            const response = await fetch(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${access_token}`);
            const profile = await response.json();
            return Promise.resolve(profile);
        } catch (error) {
            log({ message: "Google connect error;", data: error, level: "error" });
            return Promise.reject(error);
        }
    }

    const connect = async ({ access_token }) => {
        try {
            const profile = await getProfile({ access_token });
            const newGoogleEmail = profile.email;
            setRefs({ access_token, email: newGoogleEmail });
        } catch (error) {
            log({ message: "Google connect error;", data: error, level: "error" });
            return Promise.reject(error);
        }
    }

    const disconnect = async () => {
        try {
            revokeAccessToken(accessToken.value);
            setRefs({ access_token: "", email: "" });
        } catch (error) {
            log({ message: "Google disconnect error;", data: error, level: "error" });
            return Promise.reject(error);
        }
    }

    const onSuccess = async (response) => {
        const { access_token } = response;
        if (!access_token)
            return Promise.reject(`Google login error: No creditionals`);

        try {
            await connect({ access_token })
        } catch (error) {
            return Promise.reject(error);
        }
    }

    const onError = (error) => {
        log({ message: "Google Login failed", data: error, level: "error" });
        throw new Error('Google login error');
    }

    const { isReady, login } = useTokenClient({
        onSuccess,
        onError,
        scope: 'https://www.googleapis.com/auth/fitness.activity.read https://www.googleapis.com/auth/fitness.activity.write',
    });

    return {
        accessToken,
        googleEmail,
        isReady,
        login,
        disconnect,
        getProfile,
    };
}