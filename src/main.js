import { createApp } from 'vue'
import App from './App.vue'
import store from './store';
import router from './router';
import './assets/main.css';
import GoogleSignInPlugin from 'vue3-google-signin'

const app = createApp(App);
app.use(router);
app.use(store);
app.use(GoogleSignInPlugin, {
    clientId: import.meta.env.VITE_GOOGLE_CLIENT_ID,
});
app.mount('#app');
